import os


def keys():
    if not os.path.exists('keys_enc'):
        os.makedirs('keys_enc')
    f = open("Keys", "r")
    content = f.read().replace('KEYS=', '').split(",")
    for i in range(len(content)):
        index, key = content[i].split(":")
        w = open("keys_enc/env_file_%i" % int(index), "w+")
        w.write(key)
        w.close()


if __name__ == "__main__":
    keys()
